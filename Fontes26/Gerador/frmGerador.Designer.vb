﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGerador
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGerador))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkModulos = New System.Windows.Forms.CheckedListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtConfig = New System.Windows.Forms.TextBox()
        Me.cboClientes = New System.Windows.Forms.ComboBox()
        Me.grdMaquinas = New System.Windows.Forms.DataGridView()
        Me.Nome = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descricao = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Chave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Id_Maquina = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Id_Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtQt_TipoSubItem = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtQt_TipoProcesso = New System.Windows.Forms.TextBox()
        Me.txtQt_Fluxo = New System.Windows.Forms.TextBox()
        Me.txtDt_ValidadeSuporte = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtDt_ValidadeLicenca = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNu_Licencas = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNmCliente = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.grdMaquinas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'chkModulos
        '
        resources.ApplyResources(Me.chkModulos, "chkModulos")
        Me.chkModulos.CheckOnClick = True
        Me.chkModulos.FormattingEnabled = True
        Me.chkModulos.Name = "chkModulos"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'txtConfig
        '
        resources.ApplyResources(Me.txtConfig, "txtConfig")
        Me.txtConfig.Name = "txtConfig"
        Me.txtConfig.ReadOnly = True
        '
        'cboClientes
        '
        resources.ApplyResources(Me.cboClientes, "cboClientes")
        Me.cboClientes.DisplayMember = "Nm_Cliente"
        Me.cboClientes.FormattingEnabled = True
        Me.cboClientes.Name = "cboClientes"
        Me.cboClientes.ValueMember = "Id_cliente"
        '
        'grdMaquinas
        '
        resources.ApplyResources(Me.grdMaquinas, "grdMaquinas")
        Me.grdMaquinas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMaquinas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nome, Me.Descricao, Me.Chave, Me.Id_Maquina, Me.Id_Cliente, Me.Cliente})
        Me.grdMaquinas.Name = "grdMaquinas"
        '
        'Nome
        '
        Me.Nome.DataPropertyName = "Nm_Maquina"
        resources.ApplyResources(Me.Nome, "Nome")
        Me.Nome.Name = "Nome"
        '
        'Descricao
        '
        Me.Descricao.DataPropertyName = "Ds_Maquina"
        resources.ApplyResources(Me.Descricao, "Descricao")
        Me.Descricao.Name = "Descricao"
        '
        'Chave
        '
        Me.Chave.DataPropertyName = "Cd_Maquina"
        resources.ApplyResources(Me.Chave, "Chave")
        Me.Chave.Name = "Chave"
        '
        'Id_Maquina
        '
        Me.Id_Maquina.DataPropertyName = "Id_Maquina"
        resources.ApplyResources(Me.Id_Maquina, "Id_Maquina")
        Me.Id_Maquina.Name = "Id_Maquina"
        '
        'Id_Cliente
        '
        Me.Id_Cliente.DataPropertyName = "Id_Cliente"
        resources.ApplyResources(Me.Id_Cliente, "Id_Cliente")
        Me.Id_Cliente.Name = "Id_Cliente"
        '
        'Cliente
        '
        Me.Cliente.DataPropertyName = "Cliente"
        resources.ApplyResources(Me.Cliente, "Cliente")
        Me.Cliente.Name = "Cliente"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'GroupBox1
        '
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtQt_TipoSubItem)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtQt_TipoProcesso)
        Me.GroupBox1.Controls.Add(Me.txtQt_Fluxo)
        Me.GroupBox1.Controls.Add(Me.txtDt_ValidadeSuporte)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtDt_ValidadeLicenca)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtNu_Licencas)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtNmCliente)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'txtQt_TipoSubItem
        '
        resources.ApplyResources(Me.txtQt_TipoSubItem, "txtQt_TipoSubItem")
        Me.txtQt_TipoSubItem.Name = "txtQt_TipoSubItem"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'txtQt_TipoProcesso
        '
        resources.ApplyResources(Me.txtQt_TipoProcesso, "txtQt_TipoProcesso")
        Me.txtQt_TipoProcesso.Name = "txtQt_TipoProcesso"
        '
        'txtQt_Fluxo
        '
        resources.ApplyResources(Me.txtQt_Fluxo, "txtQt_Fluxo")
        Me.txtQt_Fluxo.Name = "txtQt_Fluxo"
        '
        'txtDt_ValidadeSuporte
        '
        resources.ApplyResources(Me.txtDt_ValidadeSuporte, "txtDt_ValidadeSuporte")
        Me.txtDt_ValidadeSuporte.Name = "txtDt_ValidadeSuporte"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'txtDt_ValidadeLicenca
        '
        resources.ApplyResources(Me.txtDt_ValidadeLicenca, "txtDt_ValidadeLicenca")
        Me.txtDt_ValidadeLicenca.Name = "txtDt_ValidadeLicenca"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'txtNu_Licencas
        '
        resources.ApplyResources(Me.txtNu_Licencas, "txtNu_Licencas")
        Me.txtNu_Licencas.Name = "txtNu_Licencas"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'txtNmCliente
        '
        resources.ApplyResources(Me.txtNmCliente, "txtNmCliente")
        Me.txtNmCliente.Name = "txtNmCliente"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'frmGerador
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.grdMaquinas)
        Me.Controls.Add(Me.cboClientes)
        Me.Controls.Add(Me.txtConfig)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkModulos)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmGerador"
        CType(Me.grdMaquinas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkModulos As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtConfig As System.Windows.Forms.TextBox
    Friend WithEvents cboClientes As System.Windows.Forms.ComboBox
    Friend WithEvents grdMaquinas As System.Windows.Forms.DataGridView
    Friend WithEvents Nome As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descricao As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Chave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Id_Maquina As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Id_Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDt_ValidadeSuporte As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDt_ValidadeLicenca As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNu_Licencas As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNmCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtQt_TipoProcesso As System.Windows.Forms.TextBox
    Friend WithEvents txtQt_Fluxo As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtQt_TipoSubItem As System.Windows.Forms.TextBox

End Class
