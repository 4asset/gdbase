﻿Imports System.Reflection

Public Class frmCodificador

    Private Sub txtEntrada_TextChanged(sender As Object, e As EventArgs) Handles txtEntrada.TextChanged

        txtSaida.Text = Codificador.Codificar(txtEntrada.Text)
        TextBox1.Text = Codificador.Decodificar(txtSaida.Text)

    End Sub
   
    Private Sub txtSaida_TextChanged(sender As Object, e As EventArgs) Handles txtSaida.TextChanged
        TextBox1.Text = Codificador.Decodificar(txtSaida.Text.Replace(" ", ""))
    End Sub
End Class
