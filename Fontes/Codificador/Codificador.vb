﻿Imports System.Reflection

Public Class Codificador
    Friend Shared Function Codificar(s As String)
       

        Dim o As String = ""
        Dim i As Integer = 1
        For Each a As Char In s
            o += Chr(Asc(a) + i)
            i += 1
            If i > 7 Then i = 1
        Next
        Dim r As String = ""
        For Each a As Char In o
            r += Conversion.Hex(Asc(a))
        Next
        Return r


    End Function
    Friend Shared Function Decodificar(s As String) As String
        Dim r As String = ""
        For x As Integer = 1 To Len(s) Step 2

            r += Chr(CInt("&H" & Mid(s, x, 2)))

        Next

        Dim o As String = ""
        Dim i As Integer = 1
        For Each a As Char In r
            o += Chr(Asc(a) - i)
            i += 1
            If i > 7 Then i = 1
        Next
        Return o
    End Function
End Class