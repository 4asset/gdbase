﻿Imports System.Security.Cryptography


Public Class frmGerador
    Dim Modulos As List(Of Dominio)
    Dim bs As New BindingSource
    Dim ct As New NeogeoEntities
    Dim oCliente As Cliente
    Private Sub frmGerador_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            txtConfig.Enabled = False
            chkModulos.Enabled = False
            grdMaquinas.Enabled = False
            GroupBox1.Enabled = False
            Modulos = (From L In ct.Dominio Where L.Cd_GrupoDominio = "Mod" Select L).ToList

            Dim Clientes = (From C In ct.Cliente Select C).ToList

            Clientes.Insert(0, New Cliente With {.Nm_Cliente = "(escolha)", .Id_Cliente = 0})





            cboClientes.DataSource = Clientes


            AtualizaModulos()
            GroupBox1.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub AtualizaModulos()
        chkModulos.Items.Clear()
        Dim Id_ClienteSelecionado As Integer = cboClientes.SelectedValue
        For Each D In Modulos
            Dim PossuiModulo As Boolean = (From M In ct.Modulo Where M.Id_Cliente = Id_ClienteSelecionado And M.Tp_Modulo = D.Cd_Dominio).Any
            chkModulos.Items.Add(D.Nm_Dominio, PossuiModulo)

        Next
    End Sub
    Private Sub chkModulos_Click(sender As Object, e As EventArgs) Handles chkModulos.Click

    End Sub

    Private Sub chkModulos_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles chkModulos.ItemCheck

    End Sub



    Private Sub chkModulos_SelectedValueChanged(sender As Object, e As EventArgs) Handles chkModulos.SelectedValueChanged
        GeraChave()
        Dim Item As String = chkModulos.SelectedItem
        Dim Marcado = (From C In chkModulos.CheckedItems Where C = Item).Any
        Dim Id_ClienteSelecionado As Integer = cboClientes.SelectedValue
        Dim ModuloSelecionado As New Modulo
        Dim Tp_Modulo As String = (From D In ct.Dominio Where D.Nm_Dominio = Item And D.Cd_GrupoDominio = "Mod" Select D.Cd_Dominio).Single
        If Marcado Then
            ModuloSelecionado.Id_Cliente = Id_ClienteSelecionado
            ModuloSelecionado.Tp_Modulo = Tp_Modulo
            ct.Modulo.AddObject(ModuloSelecionado)
        Else
            ModuloSelecionado = (From M In ct.Modulo Where M.Id_Cliente = Id_ClienteSelecionado And M.Tp_Modulo = Tp_Modulo).Single
            ct.Modulo.DeleteObject(ModuloSelecionado)
        End If
        ct.SaveChanges()
    End Sub

    Private Sub chkModulos_Validated(sender As Object, e As EventArgs) Handles chkModulos.Validated

    End Sub
    Sub GeraChave()

        Dim sModulos = ""
        If cboClientes.SelectedIndex = 0 Then
            txtConfig.Text = ""
            txtConfig.Enabled = False
            chkModulos.Enabled = False
            grdMaquinas.Enabled = False
            Exit Sub
        End If
        grdMaquinas.Enabled = True
        chkModulos.Enabled = True
        txtConfig.Enabled = True
        If grdMaquinas.RowCount = 0 Then
            MessageBox.Show("Informe a identificação das Máquinas", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        For Each M In chkModulos.CheckedItems
            Dim S = (From ms In Modulos Where ms.Nm_Dominio = M.ToString And ms.Cd_GrupoDominio = "Mod" Select ms.Cd_Dominio).First
            If sModulos = "" Then
                sModulos = S
            Else
                sModulos += "," & S
            End If


        Next
        'Chave de licença de validade da licença e de suporte
        Dim LicenciadoPara As String = txtNmCliente.Text
        txtConfig.Text = "<add key=""LicenciadoPara"" value=""" & LicenciadoPara & """/>"
        txtConfig.Text += vbCrLf & "<add key=""Modulos"" value=""" & sModulos & """/>"
        Dim Id_ClienteSelecionado As Integer = cboClientes.SelectedValue

        Dim Identificacoes() As String = (From ma In ct.Maquina Where ma.Id_Cliente = Id_ClienteSelecionado Select ma.Cd_Maquina).ToArray

        txtConfig.Text += vbCrLf & "<add key=""Licencas"" value="""
        For I = 0 To Identificacoes.Count - 1
            If I > 0 Then
                txtConfig.Text += ","
            End If
            txtConfig.Text += GetHash(Identificacoes(I).Trim, sModulos).ToUpper
        Next
        txtConfig.Text += """/>"

        'Chave de licença de validade e de suporte
        Dim Quantidades_e_Validades As String = Codificar(txtNu_Licencas.Text & "|" & txtDt_ValidadeLicenca.Text & "|" & txtDt_ValidadeSuporte.Text & "|" & txtQt_Fluxo.Text & "|" & txtQt_TipoProcesso.Text & "|" & txtQt_TipoSubItem.Text)
        txtConfig.Text += vbCrLf & "<add key=""Licencas2"" value=""" & Quantidades_e_Validades & """/>"

        txtConfig.Text += vbCrLf & "<add key=""Licencas3"" value="""
        For I = 0 To Identificacoes.Count - 1
            If I > 0 Then
                txtConfig.Text += ","
            End If
            txtConfig.Text += GetHash(Identificacoes(I).Trim, LicenciadoPara & Quantidades_e_Validades).ToUpper
        Next
        txtConfig.Text += """/>"

    End Sub
    Private Function GetHash(s1 As String, s2 As String) As String
        Dim ChaveInterna = "7722766B736C7A67646F38373D3A36363739292B2A2426AB2A6D6D7A69786666"
        Dim o As New SHA1CryptoServiceProvider
        Dim ba() As Byte = System.Text.Encoding.ASCII.GetBytes(s2 & Decode(ChaveInterna) & s1)
        ba = o.ComputeHash(ba)
        Dim s3 As String = ""
        For Each b As Byte In ba
            s3 += b.ToString("x2")
        Next
        Return s3
    End Function
    Private Shared Function Decode(s As String) As String
        'decodifica uma string que foi codificada com o "Codificador"
        s = s.Replace(" ", "")
        Dim r As String = ""
        For x As Integer = 1 To Len(s) Step 2
            r += Chr(CInt("&H" & Mid(s, x, 2)))
        Next
        Dim o As String = ""
        Dim i As Integer = 1
        For Each a As Char In r
            o += Chr(Asc(a) - i)
            i += 1
            If i > 7 Then i = 1
        Next
        Return o
    End Function
    Friend Shared Function Codificar(s As String)


        Dim o As String = ""
        Dim i As Integer = 1
        For Each a As Char In s
            o += Chr(Asc(a) + i)
            i += 1
            If i > 7 Then i = 1
        Next
        Dim r As String = ""
        For Each a As Char In o
            r += Conversion.Hex(Asc(a))
        Next
        Return r


    End Function
 

    Private Sub cboClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedIndexChanged

        Dim Id_ClienteSelecionado As Integer = cboClientes.SelectedValue

        oCliente = (From C In ct.Cliente Where C.Id_Cliente = Id_ClienteSelecionado).SingleOrDefault
        If oCliente IsNot Nothing Then
            txtNmCliente.Text = oCliente.Nm_Cliente
            txtNu_Licencas.Text = oCliente.Nu_Licencas
            If oCliente.Dt_ValidadeLicenca Is Nothing Then
                txtDt_ValidadeLicenca.Text = ""
            Else
                txtDt_ValidadeLicenca.Text = oCliente.Dt_ValidadeLicenca
            End If
            If oCliente.Dt_ValidadeSuporte Is Nothing Then
                txtDt_ValidadeSuporte.Text = ""
            Else
                txtDt_ValidadeSuporte.Text = oCliente.Dt_ValidadeSuporte
            End If
            If oCliente.Qt_TipoProcesso Is Nothing Then
                txtQt_TipoProcesso.Text = ""
            Else
                txtQt_TipoProcesso.Text = oCliente.Qt_TipoProcesso
            End If
            If oCliente.Qt_TipoSubItem Is Nothing Then
                txtQt_TipoSubItem.Text = ""
            Else
                txtQt_TipoSubItem.Text = oCliente.Qt_TipoSubItem
            End If
            If oCliente.Qt_Fluxo Is Nothing Then
                txtQt_Fluxo.Text = ""
            Else
                txtQt_Fluxo.Text = oCliente.Qt_Fluxo
            End If

            GroupBox1.Enabled = True
        Else
            txtNmCliente.Text = ""
            txtNu_Licencas.Text = ""
            txtDt_ValidadeSuporte.Text = ""
            txtDt_ValidadeSuporte.Text = ""
            txtQt_Fluxo.Text = ""
            txtQt_TipoSubItem.Text = ""
            txtQt_TipoProcesso.Text = ""
            GroupBox1.Enabled = False
        End If

        Dim Maquinas = From Ma In ct.Maquina Where Ma.Id_Cliente = Id_ClienteSelecionado
                        Select Ma


        bs.DataSource = Maquinas

        bs.AllowNew = True
        grdMaquinas.DataSource = bs
        AtualizaModulos()
        GeraChave()

    End Sub

    Private Sub grdMaquinas_RowValidated(sender As Object, e As DataGridViewCellEventArgs) Handles grdMaquinas.RowValidated
        GeraChave()

    End Sub

    Private Sub grdMaquinas_RowValidating(sender As Object, e As DataGridViewCellCancelEventArgs) Handles grdMaquinas.RowValidating
        Dim reg As Maquina = bs.Current
        If (reg.Nm_Maquina Is Nothing OrElse reg.Nm_Maquina.Trim = "") And (reg.Cd_Maquina Is Nothing OrElse reg.Cd_Maquina.Trim = "") Then

            Exit Sub
        End If
        If reg.Nm_Maquina Is Nothing OrElse reg.Nm_Maquina.Trim = "" Then
            MessageBox.Show("Defina o nome da máquina")
            e.Cancel = True
        End If
        If reg.Cd_Maquina Is Nothing OrElse reg.Cd_Maquina.Trim = "" Then
            MessageBox.Show("Informe a identificação da máquina")
            e.Cancel = True
        End If

        If reg.Id_Cliente = 0 Then
            reg.Id_Cliente = cboClientes.SelectedValue
        End If

        bs.EndEdit()
        Try
            ct.SaveChanges()
        Catch ex As Exception

        End Try
        GeraChave()
        
    End Sub

   

    Private Sub txtNmCliente_Validated(sender As Object, e As EventArgs) Handles txtNmCliente.Validated
        oCliente.Nm_Cliente = txtNmCliente.Text
        ct.SaveChanges()
        GeraChave()
    End Sub

    Private Sub txtNu_Licencas_Validated(sender As Object, e As EventArgs) Handles txtNu_Licencas.Validated
        oCliente.Nu_Licencas = txtNu_Licencas.Text
        ct.SaveChanges()
        GeraChave()
    End Sub

    Private Sub txtDt_ValidadeLicenca_Validated(sender As Object, e As EventArgs) Handles txtDt_ValidadeLicenca.Validated
        If txtDt_ValidadeLicenca.Text.Trim = "" Then
            oCliente.Dt_ValidadeLicenca = Nothing
        Else

            oCliente.Dt_ValidadeLicenca = txtDt_ValidadeLicenca.Text
        End If

        ct.SaveChanges()
        GeraChave()
    End Sub

    Private Sub txtDt_ValidadeSuporte_Validated(sender As Object, e As EventArgs) Handles txtDt_ValidadeSuporte.Validated

        If txtDt_ValidadeSuporte.Text.Trim = "" Then
            oCliente.Dt_ValidadeSuporte = Nothing
        Else

            oCliente.Dt_ValidadeSuporte = txtDt_ValidadeSuporte.Text
        End If

        ct.SaveChanges()
        GeraChave()
    End Sub
    Private Sub txtQt_Fluxo_Validated(sender As Object, e As EventArgs) Handles txtQt_Fluxo.Validated
        If txtQt_Fluxo.Text.Trim = "" Then
            oCliente.Qt_Fluxo = Nothing
        Else

            oCliente.Qt_Fluxo = txtQt_Fluxo.Text
        End If

        ct.SaveChanges()
        GeraChave()
    End Sub

    Private Sub txtQt_TipoProcesso_Validated(sender As Object, e As EventArgs) Handles txtQt_TipoProcesso.Validated
        If txtQt_TipoProcesso.Text.Trim = "" Then
            oCliente.Qt_TipoProcesso = Nothing
        Else

            oCliente.Qt_TipoProcesso = txtQt_TipoProcesso.Text
        End If

        ct.SaveChanges()
        GeraChave()
    End Sub

    Private Sub txtQt_TipoSubItem_Validated(sender As Object, e As EventArgs) Handles txtQt_TipoSubItem.Validated
        If txtQt_TipoSubItem.Text.Trim = "" Then
            oCliente.Qt_TipoSubItem = Nothing
        Else

            oCliente.Qt_TipoSubItem = txtQt_TipoSubItem.Text
        End If

        ct.SaveChanges()
        GeraChave()
    End Sub
    Private Sub txtNmCliente_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtNmCliente.Validating
        If txtNmCliente.Text.Trim = "" Then
            MessageBox.Show("Nome inválido!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtNu_Licencas_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtNu_Licencas.Validating
        If txtNu_Licencas.Text.Trim = "" OrElse IsNumeric(txtNu_Licencas.Text) = False Then
            MessageBox.Show("Nº de Licenças inválido!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtDt_ValidadeLicenca_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtDt_ValidadeLicenca.Validating
        If txtDt_ValidadeLicenca.Text.Trim <> "" AndAlso IsDate(txtDt_ValidadeLicenca.Text) = False Then
            MessageBox.Show("Data de Validade das licenças inválida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtDt_ValidadeSuporte_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtDt_ValidadeSuporte.Validating
        If txtDt_ValidadeSuporte.Text.Trim = "" OrElse IsDate(txtDt_ValidadeSuporte.Text) = False Then
            MessageBox.Show("Data de Validade do suporte inválida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    
    Private Sub txtQt_Fluxo_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtQt_Fluxo.Validating
        If txtQt_Fluxo.Text.Trim <> "" And IsNumeric(txtQt_Fluxo.Text) = False Then
            MessageBox.Show("Quantidade de Fluxo inválida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtQt_TipoProcesso_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtQt_TipoProcesso.Validating
        If txtQt_TipoProcesso.Text.Trim <> "" And IsNumeric(txtQt_TipoProcesso.Text) = False Then
            MessageBox.Show("Quantidade de TipoProcesso inválida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtQt_TipoSubItem_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtQt_TipoSubItem.Validating
        If txtQt_TipoSubItem.Text.Trim <> "" And IsNumeric(txtQt_TipoSubItem.Text) = False Then
            MessageBox.Show("Quantidade de TipoSubItem inválida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            e.Cancel = True
        End If
    End Sub
End Class
